#Whoeps, 31st Aug 2021


invfile = open('data/real/inversion/invids.txt')
esnpfile = open('data/esnps/esnpids.txt')

INVS = [line[:-1] for line in invfile]
ESNPS = [line[:-1] for line in esnpfile]

dict_invs = dict(zip(INVS, range(1,len(INVS)+1)))
dict_esnps = dict(zip(ESNPS, range(1,len(ESNPS)+1)))


rule all:
    input: 
       #expand('res/res_{inv}.tsv', inv=INVS),
       expand('esnp/res_{esnp}.tsv', esnp=ESNPS)

rule run_one_inv:
    input:
        invfile = 'data/real/inversion/hg38_v1.3_gts_bulk.tsv',
        expfile = 'data/real/expression/count_allelic_exons.txt'
    output:
        'res/res_{inv}.tsv'
    log:
        "logs/{inv}.log"
    params:
        number = lambda wc: dict_invs[wc.inv]
    shell:
        """
        Rscript R/adeseq_inv.R -i {input.invfile} -e {input.expfile} -n {params.number} -m res
        """

rule run_one_esnp:
    input:
        invfile = 'data/esnps/esnps_invemulate_nonred.tsv',
        expfile = 'data/real/expression/count_allelic_exons.txt'
    output:
        'esnp/res_{esnp}.tsv'
    log:
        "logs/esnp_{esnp}.log"
    params:
        number = lambda wc: dict_esnps[wc.esnp]
    shell:
        """
        /g/korbel/hoeps/anaconda3/envs/r-environment/bin/Rscript R/adeseq_inv.R -i {input.invfile} -e {input.expfile} -n {params.number} -m esnp
        """

