#!/bin/bash
set -euo pipefail

awk '{FS=OFS="\t"} NR==1 {print "seqnames","start","end","width","strand","orig_ID","len","valid_bins","verdict","nref","nhet","nhom","ninvdup","ncomplex",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,"misorient","CALLERSET_LIST","lowConfCount","noreadsCount","categ"}' ../data/esnps/testable_variants_with_GT_and_affected_gene_50reads.txt > ../data/esnps/esnps_invemulate.tsv
awk '{FS=OFS="\t"} NR>1 {print $36,$37,$38, ".",".", $36"-SNP-"$37, ".",".",".",".",".",".",".",".",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,".",".",".",".","."}' ../data/esnps/testable_variants_with_GT_and_affected_gene_50reads.txt >> ../data/esnps/esnps_invemulate.tsv

awk '{FS=OFS="\t"} !seen[$6]++' ../data/esnps/esnps_invemulate.tsv > ../data/esnps/esnps_invemulate_nonred.tsv

