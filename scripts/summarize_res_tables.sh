#!/bin/bash
set -euo pipefail

dir="esnp"
#Script to concatenate the individual deseq2 runs into one big file. 
awk 'FNR==1 && NR!=1 { while (/^<header>/) getline; } 1 {print}' ../${dir}/res_*.tsv > all.txt
awk 'BEGIN {FS=OFS="\t"} ((!seen[$0]++) && ($1~/[1-9XYa-zA-Z].*/)) {print $0}' all.txt > ../${dir}/merged.txt
awk 'BEGIN {FS=OFS="\t"} (NR==1 || $11<1)' ../${dir}/merged.txt > ../${dir}/merged_interest.txt

rm all.txt
#
